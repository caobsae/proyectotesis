Pack of 2 modular characters(male and female) that I made for my game.

They are all in range 2-3k tris and textures 512x512 - 1024x1024 png.

They are rigged and have various animations(around 45 animation each).

Each are packed in separated blend file.

Hope you find them useful ;)

Copyright/Attribution Notice: 
You don't need to credit me, but if you like you can mention me as System G6 or Qoma.
