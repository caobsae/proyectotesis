using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PantallaCarga{
    public static string siguienteNivel;

    public static void  LoadLever(string nombre){
        siguienteNivel = nombre;
        SceneManager.LoadScene("PantallaCarga");
    }
}
