using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParteCuerpo : MonoBehaviour{

    [Header("Variable de linkeado")]
    public GameObject padre;

    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update(){
        
    }

    public void SetPadre(GameObject obj){
        padre = obj;
    }

    private void OnMouseDown(){
        padre.GetComponentInParent<EnemieGeneration>().RecibirDaño(Random.Range(0, 25));
    }
}
