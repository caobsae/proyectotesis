using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class NivelACargar : MonoBehaviour{
    // Start is called before the first frame update
    void Start(){
        string nivelACargar = PantallaCarga.siguienteNivel;
        StartCoroutine(this.CargarNivel(nivelACargar));
    }

    IEnumerator CargarNivel(string nivel){
        AsyncOperation operation = SceneManager.LoadSceneAsync(nivel);
        while(operation.isDone  == false){
            yield return null;
        }
    }
}
