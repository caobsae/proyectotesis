using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour{
    [Header("Variables de bala")]
    public float tiempoDeVida;
    // Start is called before the first frame update
    void Start(){
        tiempoDeVida = 5f;
        Destroy(gameObject, tiempoDeVida);
    }

    void OnCollisionEnter(Collision other) {
        Destroy(this.gameObject);
    }
}
