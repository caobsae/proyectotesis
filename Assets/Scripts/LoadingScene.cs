using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoadingScene : MonoBehaviour{
    string[] consejos;
    public TextMeshProUGUI textoConsejos;
    // Start is called before the first frame update
    void Start(){
        CrearConsejos();
        textoConsejos.text = consejos[Random.Range(0, 4)];
        InvokeRepeating("ActualizarConsejos", 0.0f, 3.5f);
    }


    void ActualizarConsejos(){
        textoConsejos.text = consejos[Random.Range(0, 4)];
    }

    void CrearConsejos(){
        consejos = new string[4];
        consejos[0] = "Las partes de metal de los enemigos son más vulnerables a balas electricas";
        consejos[1] = "Las balas con veneno ignoran la armadura";
        consejos[2] = "lorem ipsum";
        consejos[3] = "En Sabah Nur";

    }
}
