using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour{

    public enum EstadoJugador{
        IDLE,
        AGACHADO,
        CAMINANDO,
        APUNTANDO,
        CORRIENDO,
        APUNTARCAMINANDO,
        RECARGAR,
        RECARGARCAMINANDO,
        RECARGARCORRIENDO,
        RECARGARAGACHADO
    }

    [Header("Variables de movimiento")]
    public float velocidadCaminar;
    public float velocidadCorrer;
    public float velocidadCaminarAtras;
    public float velocidadCaminarApuntando;
    public float velocidadRotacion;
    public float velocidadRotacionCaminando;
    public float x;
    public float y;
    public bool caminando;
    public bool corriendo;
    public bool apuntando;
    public bool agachado;
    public bool apuntarCaminando;
    public bool recargar;
    public EstadoJugador estado;

    Animator miAnim;

    [Header("Variables de camara")]
    public Cinemachine.AxisState xAxis;
    public Cinemachine.AxisState yAxis;
    public Transform camaraASeguir;
    public CinemachineVirtualCamera vCam;
    public float zoomFOV;
    public float regularFOV;
    public float actualFOV;
    public float velocidadFOV;

    [Header("Variables de apuntado")]
    public Transform posicionApuntado;
    public float velocidadApuntado;
    public float sensibilidadApuntado;

    LayerMask aimMask;
    public PlayerInput inputJugador;
    public ControladorArma arma;

    // Start is called before the first frame update
    void Start(){
        //Componentes del jugador
        miAnim = GetComponent<Animator>();
        inputJugador = GetComponent<PlayerInput>();
        //Movimiento del jugador
        velocidadCaminar = 5.0f;
        velocidadCorrer = 7.0f;
        velocidadCaminarAtras = 2.0f;
        velocidadCaminarApuntando = 2.0f;
        velocidadRotacionCaminando = 100.0f;
        velocidadRotacion = 200.0f;
        //Estado del jugador
        caminando = false;
        corriendo = false;
        apuntando = false;
        agachado = false;
        apuntarCaminando = false;
        estado = EstadoJugador.IDLE;
        //Sistema del juego
        Cursor.lockState = CursorLockMode.Locked;
        //Variable de cámara
        zoomFOV = 40;
        vCam = GetComponentInChildren<CinemachineVirtualCamera>();
        regularFOV = vCam.m_Lens.FieldOfView;
        velocidadFOV = 10;
        //Variables de apuntado
        velocidadApuntado = 20;
        sensibilidadApuntado = .3f;
        SetArma();
    }

    // Update is called once per frame
    void Update(){
        
        vCam.m_Lens.FieldOfView = Mathf.Lerp(vCam.m_Lens.FieldOfView, actualFOV, velocidadFOV * Time.deltaTime);
        Vector2 centroPantalla = new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
        Ray ray = Camera.main.ScreenPointToRay(centroPantalla);

        if(Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity)){
            posicionApuntado.position = Vector3.Lerp(posicionApuntado.position, hit.point, velocidadApuntado * Time.deltaTime);
        }else{
            posicionApuntado.position = Vector3.Lerp(posicionApuntado.position, ray.GetPoint(1000), velocidadApuntado * Time.deltaTime);
        }
        InputCamara();
        ActualizacionEstados();
        MovimientoJugador();
        ActualizarAnimaciones();
    }

    void LateUpdate() {
        camaraASeguir.localEulerAngles = new Vector3(yAxis.Value, camaraASeguir.localEulerAngles.y, camaraASeguir.localEulerAngles.z);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, xAxis.Value, transform.eulerAngles.z);
        
    }

    void InputCamara(){
        if(estado == EstadoJugador.APUNTANDO || estado == EstadoJugador.APUNTARCAMINANDO){
            xAxis.Update(Time.deltaTime * sensibilidadApuntado);
            yAxis.Update(Time.deltaTime * sensibilidadApuntado);
        }else{
            xAxis.Update(Time.deltaTime);
            yAxis.Update(Time.deltaTime);
        }
    }

    private void ActualizacionEstados(){
        //x = Input.GetAxis("Horizontal");
        //y = Input.GetAxis("Vertical");
        x = inputJugador.actions["Moverse"].ReadValue<Vector2>().x;
        y = inputJugador.actions["Moverse"].ReadValue<Vector2>().y;

        if(estado == EstadoJugador.IDLE || estado == EstadoJugador.RECARGAR){
            if(x < 0 || x > 0 || y < 0 || y > 0){
                if(recargar){
                    estado = EstadoJugador.RECARGARCAMINANDO;
                }else{
                    estado = EstadoJugador.CAMINANDO;
                }
            }else if (inputJugador.actions["Apuntar"].IsPressed()){
                estado = EstadoJugador.APUNTANDO;
            }else if (inputJugador.actions["Agacharse"].IsPressed()){
                estado = EstadoJugador.AGACHADO;
            }else if (inputJugador.actions["Recargar"].IsPressed()){
                estado = EstadoJugador.RECARGAR;
            }
        }else if(estado == EstadoJugador.CAMINANDO || estado == EstadoJugador.RECARGARCAMINANDO){
            if(inputJugador.actions["Correr"].IsPressed() && y > 0){
                estado = EstadoJugador.CORRIENDO;
            }if(inputJugador.actions["Apuntar"].IsPressed()){
                estado = EstadoJugador.APUNTANDO;
            }else if (inputJugador.actions["Recargar"].WasPressedThisFrame()){
                estado = EstadoJugador.RECARGARCAMINANDO;
            }else if(x == 0 && y == 0){
                if(recargar){
                    estado = EstadoJugador.RECARGAR;
                }else{
                    estado = EstadoJugador.IDLE;
                }
            }
        }else if(estado == EstadoJugador.CORRIENDO){
            if (!inputJugador.actions["Correr"].IsPressed()){
                estado = EstadoJugador.CAMINANDO;
            }
            if(x == 0 && y == 0){
                estado = EstadoJugador.IDLE;
            }
                
        }else if(estado == EstadoJugador.AGACHADO || estado == EstadoJugador.RECARGARAGACHADO){
            if(inputJugador.actions["Apuntar"].IsPressed()){
                estado = EstadoJugador.APUNTANDO;
            }else if (inputJugador.actions["Recargar"].WasPressedThisFrame()){
                estado = EstadoJugador.RECARGARAGACHADO;
            }else if (!inputJugador.actions["Agacharse"].IsPressed()){
                estado = EstadoJugador.IDLE;
            }
        }else if(estado == EstadoJugador.APUNTANDO){
            if(x < 0 || x > 0 || y < 0 || y > 0){
                estado = EstadoJugador.APUNTARCAMINANDO;
            }else if (inputJugador.actions["Agacharse"].IsPressed() && !inputJugador.actions["Apuntar"].IsPressed()){
                estado = EstadoJugador.AGACHADO;
            }else if(!inputJugador.actions["Apuntar"].IsPressed()){
                estado = EstadoJugador.IDLE;
            }
        }else if(estado == EstadoJugador.APUNTARCAMINANDO){
            if (!inputJugador.actions["Apuntar"].IsPressed()){
                estado = EstadoJugador.IDLE;
            }else if(x == 0 && y == 0){
                estado = EstadoJugador.APUNTANDO;
            }
        }else if(estado == EstadoJugador.RECARGAR){
             if(x < 0 || x > 0 || y < 0 || y > 0){
                estado = EstadoJugador.RECARGARCAMINANDO;
             }
        }else if(estado == EstadoJugador.RECARGARCAMINANDO){
            if(x == 0 && y == 0){
                estado = EstadoJugador.RECARGAR;
            }
        }


    }

    private void MovimientoJugador(){
        switch(estado){
            case EstadoJugador.IDLE:
                caminando = false;
                corriendo = false;
                agachado = false;
                apuntando = false;
                apuntarCaminando = false;
                recargar = false;
                actualFOV = regularFOV;
                break;
            case EstadoJugador.CAMINANDO:
                caminando = true;
                corriendo = false;
                agachado = false;
                apuntando = false;
                apuntarCaminando = false;
                recargar = false;
                actualFOV = regularFOV;
                if(y == -1){
                    transform.Translate(x * velocidadCaminarApuntando * Time.deltaTime, 0, y * velocidadCaminarAtras * Time.deltaTime);
                }else{
                    transform.Translate(x * velocidadCaminarApuntando * Time.deltaTime, 0, y * velocidadCaminar * Time.deltaTime);
                }
                //transform.Rotate(0, x * velocidadRotacion * Time.deltaTime, 0);
                break;
            case EstadoJugador.CORRIENDO:
                caminando = false;
                corriendo = true;
                agachado = false;
                apuntando = false;
                apuntarCaminando = false;
                recargar = false;
                actualFOV = regularFOV;
                transform.Translate(0, 0, y * velocidadCorrer * Time.deltaTime);
                transform.Rotate(0, x * velocidadRotacion * Time.deltaTime, 0);
                break;
            case EstadoJugador.AGACHADO:
                caminando = false;
                corriendo = false;
                agachado = true;
                apuntando = false;
                apuntarCaminando = false;
                actualFOV = regularFOV;
                break;
            case EstadoJugador.APUNTANDO:
                caminando = false;
                corriendo = false;
                agachado = false;
                apuntando = true;
                apuntarCaminando = false;
                recargar = false;
                actualFOV = zoomFOV;
                break;
            case EstadoJugador.APUNTARCAMINANDO:
                caminando = false;
                corriendo = false;
                agachado = false;
                apuntando = true;
                apuntarCaminando = true;
                recargar = false;
                actualFOV = zoomFOV;
                if(y == -1){
                    transform.Translate(x * velocidadCaminarApuntando * Time.deltaTime, 0, y * velocidadCaminarAtras * Time.deltaTime);
                }else{
                    transform.Translate(x * velocidadCaminarApuntando * Time.deltaTime, 0, y * velocidadCaminarApuntando * Time.deltaTime);
                }
                transform.Rotate(0, x * velocidadRotacionCaminando * Time.deltaTime, 0);
                break;
            case EstadoJugador.RECARGAR:
                if(arma.PuedeRecargar() || recargar){
                    caminando = false;
                    corriendo = false;
                    agachado = false;
                    apuntando = false;
                    apuntarCaminando = false;
                    recargar = true;
                    arma.Recargar();
                }
                //estado = EstadoJugador.IDLE;
                break;
            case EstadoJugador.RECARGARCAMINANDO:
                if(arma.PuedeRecargar() || recargar){
                    caminando = true;
                    corriendo = false;
                    agachado = false;
                    apuntando = false;
                    apuntarCaminando = false;
                    recargar = true;
                    arma.Recargar();
                }
                if(y == -1){
                    transform.Translate(x * velocidadCaminarApuntando * Time.deltaTime, 0, y * velocidadCaminarAtras * Time.deltaTime);
                }else{
                    transform.Translate(x * velocidadCaminarApuntando * Time.deltaTime, 0, y * velocidadCaminar * Time.deltaTime);
                }
                break;
            case EstadoJugador.RECARGARAGACHADO:
                if(arma.PuedeRecargar()){
                    caminando = false;
                    corriendo = false;
                    agachado = true;
                    apuntando = false;
                    apuntarCaminando = false;
                    recargar = true;
                    arma.Recargar();
                }
                break;
            default:
                break;
        }
    }
    private void ActualizarAnimaciones(){
        miAnim.SetFloat("VelX", x);
        miAnim.SetFloat("VelY", y);
        miAnim.SetBool("Correr", corriendo);
        miAnim.SetBool("Caminar", caminando);
        miAnim.SetBool("Agachado", agachado);
        miAnim.SetBool("Apuntar", apuntando);
        miAnim.SetBool("ApuntarCaminando", apuntarCaminando);
        miAnim.SetBool("Recargando", recargar);
    }

    public void SetArma(){
        arma = GetComponentInChildren<ControladorArma>();
    }
}
