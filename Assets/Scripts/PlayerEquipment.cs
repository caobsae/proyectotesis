using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipment : MonoBehaviour{
    public int municionExtra;

    public static PlayerEquipment instance { get; private set; }

    private void Awake() { 
        // If there is an instance, and it's not me, delete myself.
        
        if (instance != null && instance != this) { 
            Destroy(this); 
        } 
        else{ 
            instance = this; 
        } 
    }

    private void Start(){
        municionExtra = 120;
    }

}
