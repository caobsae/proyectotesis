using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UbicacionMapa{
    public int x;
    public int z;

    public UbicacionMapa(int _x, int _z){
        x = _x;
        z = _z;
    }
}

public class Mazmorra : MonoBehaviour{

    public int largo = 30;//longitud en x
    public int ancho = 30;//longitud en z
    public byte [,] mapa;
    public int escala = 6;

    void Start(){
        CrearMapa();
        CrearMazmorra();
        DibujarMapa();
    }

    // Update is called once per frame
    void Update(){
        
    }

    public void CrearMapa(){
        mapa = new byte[largo, ancho];
        for(int z = 0; z < ancho; ++z){
            for(int x = 0; x < largo; ++x){
                mapa[x, z] = 1;
            }
        }
    }

    public virtual void CrearMazmorra(){
        for(int z = 0; z < ancho; ++z){
            for(int x = 0; x < largo; ++x){
                if(Random.Range(0, 100) < 50){
                    mapa[x, z] = 0;
                }
            }
        }
    }

    void DibujarMapa(){
        for(int z = 0; z < ancho; ++z){
            for(int x = 0; x < largo; ++x){
                if(mapa[x, z] == 1){
                    Vector3 pos = new Vector3(x * escala, 0, z * escala);
                    GameObject wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    wall.transform.localScale = new Vector3(escala, escala, escala);
                    wall.transform.position = pos;
                }
            }
        }
    }

    public int ContadVecinosDirectos(int x, int z){
        int contador = 0;

        if(x <= 0 || x >= largo - 1 || z <= 0 || z >= ancho - 1){
            return 5;
        }

        contador += mapa[x - 1, z] == 0 ? 1 : 0;
        contador += mapa[x + 1, z] == 0 ? 1 : 0;
        contador += mapa[x, z - 1] == 0 ? 1 : 0;
        contador += mapa[x, z + 1] == 0 ? 1 : 0;

        return contador;
    }

    public int ContadVecinosDiagonales(int x, int z){
        int contador = 0;

        if(x <= 0 || x >= largo - 1 || z <= 0 || z >= ancho - 1){
            return 5;
        }

        contador += mapa[x - 1, z - 1] == 0 ? 1 : 0;
        contador += mapa[x + 1, z - 1] == 0 ? 1 : 0;
        contador += mapa[x - 1, z + 1] == 0 ? 1 : 0;
        contador += mapa[x + 1, z + 1] == 0 ? 1 : 0;

        return contador;
    }

    public int ContadDeVecinos(int x, int z){
        return ContadVecinosDirectos(x,z) + ContadVecinosDiagonales(x,z);
    }
}
