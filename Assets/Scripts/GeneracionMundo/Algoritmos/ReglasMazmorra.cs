using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReglasMazmorra : Mazmorra{

    public override void CrearMazmorra(){
        CrearMazmorraHorizontal();
        for(int i = 0; i < 1; ++i){
            CrearMazmorraVertical();
        }
    }

    void CrearMazmorraVertical(){
        bool termino = false;
        int x = Random.Range(1, largo - 1);
        int z = 1;
        
        while(!termino){
            mapa[x, z] = 0;
            if(Random.Range(0, 100) < 50){
                x += Random.Range(-1, 2);
            }else{
                z += Random.Range(0, 2);
            }
            termino |= (x < 1 || x >= largo - 1 || z < 1 || z >= ancho - 1); 
        }
    }

    void CrearMazmorraHorizontal(){
        bool termino = false;
        int x = 1;
        int z = Random.Range(1, ancho - 1);
        
        while(!termino){
            mapa[x, z] = 0;
            if(Random.Range(0, 100) < 50){
                x += Random.Range(0, 2);
            }else{
                z += Random.Range(-1, 2);
            }
            termino |= (x < 1 || x >= largo - 1 || z < 1 || z >= ancho - 1); 
        }
    }
}
