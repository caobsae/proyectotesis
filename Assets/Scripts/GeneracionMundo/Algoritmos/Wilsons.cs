using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wilsons : Mazmorra{
    
    List<UbicacionMapa> direcciones = new List<UbicacionMapa>(){
        new UbicacionMapa(1, 0), new UbicacionMapa(0, 1), new UbicacionMapa(-1, 0), new UbicacionMapa(0, -1)
    };
    List<UbicacionMapa> ubicacionesDisponibles = new List<UbicacionMapa>();

    public override void CrearMazmorra(){
        int x = Random.Range(1, largo - 1);
        int z = Random.Range(1, ancho - 1);
        int contadorDeLoops = 0;

        mapa[x, z] = 2;

        while(GetUbicacionesDisponibles() > 1 && contadorDeLoops < (largo * ancho)){
            CrearCorredorAleatorio();
            ++contadorDeLoops;
        }
    }

    int ContadorVecinosDirectosMazmorra(int x, int z){

        int contador = 0;

        for(int i = 0; i < direcciones.Count; ++i){
            int xSiguiente = x + direcciones[i].x;
            int zSiguiente = z + direcciones[i].z;

            if(mapa[xSiguiente, zSiguiente] == 2){
                ++contador;
            }
        }

        return contador;
    }

    int GetUbicacionesDisponibles(){
        ubicacionesDisponibles.Clear();
        for(int x  = 1; x < largo - 1; ++x ){
            for(int z = 1; z < ancho - 1; ++z){
                if(ContadorVecinosDirectosMazmorra(x, z) == 0){
                    ubicacionesDisponibles.Add(new UbicacionMapa(x, z));
                }
            }
        }

        return ubicacionesDisponibles.Count;
    }

    void CrearCorredorAleatorio(){
        List<UbicacionMapa> posibleRuta = new List<UbicacionMapa>();
        int inicioAleatorio = Random.Range(0, ubicacionesDisponibles.Count);

        int xActual = ubicacionesDisponibles[inicioAleatorio].x;
        int zActual = ubicacionesDisponibles[inicioAleatorio].z;

        posibleRuta.Add(new UbicacionMapa(xActual, zActual));

        int contadorDeLoops = 0;
        bool validarCamino = false;
        while(xActual > 0 && xActual < largo -1 && zActual > 0 && zActual < ancho -1 && !validarCamino && contadorDeLoops < (ancho * largo)){
            mapa[xActual, zActual] = 0;
            
            if(ContadorVecinosDirectosMazmorra(xActual, zActual) > 1){
                posibleRuta.Remove(new UbicacionMapa(xActual, zActual));
                ++contadorDeLoops;
                break;
            }

            int direccionAleatoria = Random.Range(0, direcciones.Count);
            int posicionSiguienteX = xActual + direcciones[direccionAleatoria].x;
            int posicionSiguienteZ = zActual + direcciones[direccionAleatoria].z;

            // xActual += direcciones[direccionAleatoria].x;
            // zActual += direcciones[direccionAleatoria].z;

            if(ContadVecinosDirectos(posicionSiguienteX, posicionSiguienteZ) < 2){
                xActual = posicionSiguienteX;
                zActual = posicionSiguienteZ;
                posibleRuta.Add(new UbicacionMapa(xActual, zActual));
            }

            validarCamino = ContadorVecinosDirectosMazmorra(xActual, zActual) == 1;

            ++contadorDeLoops;
        }

        if(validarCamino){
            mapa[xActual, zActual] = 0;
            Debug.Log("Camino Encontrado");
            foreach(UbicacionMapa ruta in posibleRuta){
                mapa[ruta.x, ruta.z] = 2;
            }
            posibleRuta.Clear();
        }else{
            foreach(UbicacionMapa ruta in posibleRuta){
                mapa[ruta.x, ruta.z] = 1;
            }
            posibleRuta.Clear();
        }
    }
}
