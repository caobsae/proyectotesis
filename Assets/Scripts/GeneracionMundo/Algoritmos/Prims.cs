using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prims : Mazmorra{
    public override void CrearMazmorra(){
        int x = largo / 2;
        int z = ancho / 2;

        mapa[x, z] = 0;

        List<UbicacionMapa> paredes = new List<UbicacionMapa>();
        paredes.Add(new UbicacionMapa(x + 1, z));
        paredes.Add(new UbicacionMapa(x - 1, z));
        paredes.Add(new UbicacionMapa(x, z + 1));
        paredes.Add(new UbicacionMapa(x, z - 1));

        int contadorDeLoops = 0;//Truco para saber si hay loops infinitos y tener una salida

        while(paredes.Count > 0 && contadorDeLoops < 5000){
            int paredTempAleatoria = Random.Range(0, paredes.Count);
            x = paredes[paredTempAleatoria].x;
            z = paredes[paredTempAleatoria].z;
            paredes.RemoveAt(paredTempAleatoria);
            if(ContadVecinosDirectos(x, z) == 1){
                mapa[x, z] = 0;
                paredes.Add(new UbicacionMapa(x + 1, z));
                paredes.Add(new UbicacionMapa(x - 1, z));
                paredes.Add(new UbicacionMapa(x, z + 1));
                paredes.Add(new UbicacionMapa(x, z - 1));
            }
            ++contadorDeLoops;
        }
    }
}
