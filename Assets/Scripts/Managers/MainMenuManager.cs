using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour{
    struct DatosAnimacion{
        public string nombreAnimacion;
        public bool estadoAnimacion;

        public DatosAnimacion(string n, bool e){
            nombreAnimacion = n;
            estadoAnimacion = e;
        }
    }
    public enum ESTADOMENUPRINCIPAL {
        MENUPRINCIPAL,
        NUEVAPARTIDA,
        CONTINUARPARTIDA,
        CREDITOS,
        SALIR
    }

    [SerializeField] Button[] BotonesMenuPrincipal;
    [SerializeField] Button[] BotonesNuevaPartida;
    [SerializeField] Animator miAnim;
    [SerializeField] GameObject validador;
    [SerializeField] GameObject creditos;

    public ESTADOMENUPRINCIPAL estadoMenuPrincipal;

    DatosAnimacion[] animaciones;

    public static MainMenuManager instance { get; private set; }
    private void Awake() { 
        // If there is an instance, and it's not me, delete myself.
        
        if (instance != null && instance != this) { 
            Destroy(this); 
        } 
        else{ 
            instance = this; 
        } 
    }

    // Start is called before the first frame update
    void Start(){
        estadoMenuPrincipal = ESTADOMENUPRINCIPAL.MENUPRINCIPAL;
        animaciones = new DatosAnimacion[4];
        CrearDatosAnimaciones();
        //Revisar si hay datos de un juego, si es asi habilitar el botón de continuar partida.

    }

    // Update is called once per frame
    void Update(){
        ManejadorBotones();
    }

    void ManejadorBotones(){
        switch(estadoMenuPrincipal){
            case ESTADOMENUPRINCIPAL.MENUPRINCIPAL:
                ActualizarAnimaciones(-1);
                break;
            case ESTADOMENUPRINCIPAL.CONTINUARPARTIDA:
                ActualizarAnimaciones(0);
                break;
            case ESTADOMENUPRINCIPAL.NUEVAPARTIDA:
                ActualizarAnimaciones(1);
                break;
            case ESTADOMENUPRINCIPAL.CREDITOS:
                ActualizarAnimaciones(2);
                creditos.SetActive(true);
                if(Input.anyKeyDown){
                    estadoMenuPrincipal = ESTADOMENUPRINCIPAL.MENUPRINCIPAL;
                    creditos.GetComponentInChildren<CreditosScript>(true).ResetearCreditos();
                    creditos.SetActive(false);
                }  
                break;
            case ESTADOMENUPRINCIPAL.SALIR:
                ActualizarAnimaciones(3);
                validador.SetActive(true);
                break;
            default:
                break;
        }
    }

    void CrearDatosAnimaciones(){
        animaciones[0] = new DatosAnimacion("Continuar Partida", false);
        animaciones[1] = new DatosAnimacion("Nueva Partida", false);
        animaciones[2] = new DatosAnimacion("Creditos", false);
        animaciones[3] = new DatosAnimacion("Salir", false);
    }

    public void NuevaPartida(){
        estadoMenuPrincipal = ESTADOMENUPRINCIPAL.NUEVAPARTIDA;
    }

    public void MaineMenu(){
        validador.SetActive(false);
        estadoMenuPrincipal = ESTADOMENUPRINCIPAL.MENUPRINCIPAL;
    }

    public void ContinuarPartida(){
        //Validar si hay datos guardados, si los hay 
        estadoMenuPrincipal = ESTADOMENUPRINCIPAL.CONTINUARPARTIDA;

    }

    public void Creditos(){
        estadoMenuPrincipal = ESTADOMENUPRINCIPAL.CREDITOS;
    }

    public void SalirJuego(){
        estadoMenuPrincipal = ESTADOMENUPRINCIPAL.SALIR;
    }

    public void ActualizarAnimaciones(int posicion){
        for(int i = 0; i < 4; ++i){
            if(i != posicion){
                animaciones[i].estadoAnimacion = false;
            }else{
                animaciones[i].estadoAnimacion = true;
            }

            miAnim.SetBool(animaciones[i].nombreAnimacion, animaciones[i].estadoAnimacion);
        }

    }

    public void CargarJuego(){
        SceneManager.LoadScene("LoadScene", LoadSceneMode.Additive);
    }

    public void Salir(){
        Application.Quit();
    }
}
