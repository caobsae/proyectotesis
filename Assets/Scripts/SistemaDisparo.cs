using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemaDisparo : MonoBehaviour{
    public GameObject pivote;
    public int distance = 1000;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update(){
        RaycastHit hit;
        if(Input.GetMouseButtonDown(0)){
            if(Physics.Raycast(pivote.transform.position, pivote.transform.forward, out hit, distance)){
                Debug.DrawLine(pivote.transform.position, hit.point, Color.red);
            }
        }
        
    }
}
