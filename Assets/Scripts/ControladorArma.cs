using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class ControladorArma : MonoBehaviour{


    [Header("Variables de disparo")]
    public float cadenciaDisparo;
    public float timerCadenciaDisparo;
    public bool esSemiAutomatica;

    [Header("Variables de bala")]
    public GameObject bala;
    public Transform pivote;
    public PlayerMovement player;
    public float velocidadBala;
    public int balasPorDisparo;
    public PlayerInput inputJugador;

    [Header("Variables Arma")]
    public int municionEnCargador;
    public int municionMaxima;
    public bool debeRecargar;

    private AudioSource audio;

    // Start is called before the first frame update
    void Start(){
        municionMaxima = 35;
        municionEnCargador = municionMaxima;
        velocidadBala = 300;
        balasPorDisparo = 1;
        cadenciaDisparo = 0.25f;
        esSemiAutomatica = true;
        debeRecargar = false;
        inputJugador = GetComponentInParent<PlayerInput>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update(){
        if(!debeRecargar){
            if(PuedeDisparar()){
                Disparar();
            }
        }else{
            Recargar();
        }
        Debug.Log(municionEnCargador);
    }

    bool PuedeDisparar(){
        timerCadenciaDisparo += Time.deltaTime;
        if(municionEnCargador == 0){//Ver si hay balas en el cartucho
            return false;
        }
        if(esSemiAutomatica && inputJugador.actions["Disparar"].WasPressedThisFrame()){//Si es semi y se presiona el boton en este frame dispara
            if(municionEnCargador <= 0){
                debeRecargar = true;
                return false;
            }

            return true;
        }
        if(timerCadenciaDisparo < cadenciaDisparo){//Revisar si ya paso el tiempo entre disparos
            return false;
        }
        if(!esSemiAutomatica && inputJugador.actions["Apuntar"].IsPressed() && inputJugador.actions["Disparar"].IsPressed()){//Si es automatica y se esta presionando el boton dispara
            if(municionEnCargador <= 0){
                debeRecargar = true;
                return false;
            }
            
            return true;
        }
        return false;
    }

    public bool PuedeRecargar(){
        if(inputJugador.actions["Recargar"].WasPressedThisFrame() && (municionEnCargador < municionMaxima) && PlayerEquipment.instance.municionExtra > 0){
            return true;
        }
        return false;;
    }

    void Disparar(){
        --municionEnCargador;
        timerCadenciaDisparo = 0;
        audio.Play();
        pivote.LookAt(player.posicionApuntado);
        for(int i = 0; i < balasPorDisparo; ++i){//En caso de escopetas por ejemplo que dispara mas de una bala a la vez.
            GameObject temp = Instantiate(bala, pivote.position, pivote.rotation);
            Rigidbody rb = temp.GetComponent<Rigidbody>();
            rb.AddForce(pivote.forward * velocidadBala, ForceMode.Impulse);
        }
    }

    public void Recargar(){
        Debug.Log("Recargar");
        if(PlayerEquipment.instance.municionExtra > 0){//Revisar si hay municion extra para recargar
            if(municionEnCargador < municionMaxima){//Revisar si el cargador no esta lleno
                int nuevaMunicion = municionMaxima - municionEnCargador;
                PlayerEquipment.instance.municionExtra -= nuevaMunicion;
                municionEnCargador += nuevaMunicion;
            }
        }
        debeRecargar = false;
    }
}
