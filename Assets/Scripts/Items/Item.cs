using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Item : MonoBehaviour{
    public enum Tier{
        COMUN,
        ESPECIAL,
        EPICO,
        LEGENDARIO
    }

    public enum Tipo{
        MUNICION,
        EQUIPAMIENTO,
        ARMA,
        ESPECIAL,
        HABILIDAD
    }

    public int item_ID;
    public Tier item_Tier;
    public Tipo item_Tipo;

    public virtual string Descripcion(){
        return "Debe retornar la descripcion del item";
    }
}
