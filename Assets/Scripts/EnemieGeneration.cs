using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieGeneration : MonoBehaviour{
    [Header("Pivotes")]
    public Transform pivoteBrazoIzq;
    public Transform pivoteBrazoDer; 
    public Transform pivotePiernaIzq;
    public Transform pivotePiernaDer;
    public Transform pivoteCabeza;
    [Header("Listas partes del cuerpo")]
    public GameObject tronco;
    public List<GameObject> piernasDer;
    public List<GameObject> piernasIzq;
    public List<GameObject> brazosDer;
    public List<GameObject> brazosIzq;
    public List<GameObject> cabezas;
    [Header("Partes cuerpo")]
    public GameObject brazoDer;
    public GameObject brazoIzq;
    public GameObject piernaDer;
    public GameObject piernaIzq;
    public GameObject cabeza;
    public GameObject padre;
    [Header("Variables de control")]
    public int numPersonaje;
    public int vida;
    // Start is called before the first frame update
    void Start(){
        numPersonaje = 0;
        vida = 100;
    }

    // Update is called once per frame
    void Update(){
        if (Input.GetKeyDown("space")){
            vida = 100;
            CrearPersonaje();
            ++numPersonaje;
        }
    }

    void CrearPersonaje(){
        //Generación pierna Der

        //Destruir pierna anterior en caso de haber
        if(piernaDer != null){
            Destroy(piernaDer);
        }

        //Instanciar
        int index = Random.Range(0, piernasDer.Count);
        GameObject temp = Instantiate(piernasDer[index], pivotePiernaDer.position, Quaternion.identity);
        piernaDer = temp;

        //Ubicarla como hijo en el editor
        piernaDer.transform.parent = pivotePiernaDer.transform;

        //Linkearlo con el sistema central
        piernaDer.GetComponentInParent<ParteCuerpo>().SetPadre(padre);


        //Generación pierna Izq
        if(piernaIzq != null){
            Destroy(piernaIzq);
        }
        index = Random.Range(0, piernasIzq.Count);
        temp = Instantiate(piernasIzq[index], pivotePiernaIzq.position, Quaternion.identity);
        piernaIzq = temp;
        piernaIzq.transform.parent = pivotePiernaIzq.transform;
        piernaIzq.GetComponentInParent<ParteCuerpo>().SetPadre(padre);

        //Generación brazo Der
        if(brazoDer != null){
            Destroy(brazoDer);
        }
        index = Random.Range(0, piernasDer.Count);
        temp = Instantiate(brazosDer[index], pivoteBrazoDer.position, Quaternion.identity);
        brazoDer = temp;
        brazoDer.transform.parent = pivoteBrazoDer.transform;
        brazoDer.GetComponentInParent<ParteCuerpo>().SetPadre(padre);

        //Generación brazo Izq
        if(brazoIzq != null){
            Destroy(brazoIzq);
        }
        index = Random.Range(0, piernasDer.Count);
        temp = Instantiate(brazosIzq[index], pivoteBrazoIzq.position, Quaternion.identity);
        brazoIzq = temp;
        brazoIzq.transform.parent = pivoteBrazoIzq.transform;
        brazoIzq.GetComponentInParent<ParteCuerpo>().SetPadre(padre);

        //Generación cabeza
        if(cabeza != null){
            Destroy(cabeza);
        }
        index = Random.Range(0, piernasDer.Count);
        temp = Instantiate(cabezas[index], pivoteCabeza.position, Quaternion.identity);
        cabeza = temp;
        cabeza.transform.parent = pivoteCabeza.transform;
        cabeza.GetComponentInParent<ParteCuerpo>().SetPadre(padre);
    }

    public void RecibirDaño(int daño){
        vida -= daño;
        Debug.Log("Vida: " + vida);
    }
}
